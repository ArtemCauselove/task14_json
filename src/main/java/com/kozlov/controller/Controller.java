package com.kozlov.controller;

import com.kozlov.model.MainClass;

public class Controller {
    MainClass main;
    public Controller(){
        main = new MainClass();
    }
    public void launch(){
        main.start();
    }
}
