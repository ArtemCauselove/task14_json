package com.kozlov.model;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.kozlov.View.View;
import com.kozlov.model.json.gson.JSONGsonParser;
import com.kozlov.model.json.gson.JSONGsonReader;
import com.kozlov.model.comparator.*;
import com.kozlov.model.json.jackson.JSONJacksonParser;
import com.kozlov.model.json.validator.JSONNewValidator;
import com.kozlov.model.json.validator.JSONValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

public class MainClass {
  private  ResourceBundle resourceBundle = ResourceBundle.getBundle("path");
    private static Logger logger = LogManager.getLogger(View.class);
  public  void start() {
    String pathJSON = resourceBundle.getString("json");
    String pathSCHEME = resourceBundle.getString("scheme");
    File json = new File(pathJSON);
    File schema = new File(pathSCHEME);
    JSONValidator.validateSchema(schema, json);
    try {
      JSONNewValidator.validate(json,schema);
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ProcessingException e) {
      e.printStackTrace();
    }
    JSONJacksonParser parser = new JSONJacksonParser();
    JSONGsonParser gsonParser = new JSONGsonParser();
    printList(parser.getCandieList(json));
      logger.info("1\n");
    printList(gsonParser.getCandieList(json));
      logger.info("2\n");
    try {
      JSONGsonReader gsonReader = new JSONGsonReader(json);
      printList(gsonReader.readCandie());
      logger.info("3\n");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }
  private static void printList(List<Candie> devices) {
    Collections.sort(devices, new CompareCandie());
      logger.info("JSON\n");
    for (Candie device : devices) {
        logger.info(device + "\n");
    }
  }
  private static void writeToFile(List<Candie> devices) {
    JSONGsonParser parser = new JSONGsonParser();
    parser.writeToFile(devices, new File("src/main/resources/devicesNew.json"));
  }
}
