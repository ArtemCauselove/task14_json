package com.kozlov.model.comparator;
import com.kozlov.model.Candie;
import java.util.Comparator;
public class CompareCandie implements Comparator<Candie> {
  public int compare(Candie o1, Candie o2) {
    return Integer.compare(o1.getCandieNo(), o2.getCandieNo());
  }
}
