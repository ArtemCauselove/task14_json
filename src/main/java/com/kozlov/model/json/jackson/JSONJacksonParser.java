package com.kozlov.model.json.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kozlov.model.Candie;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class JSONJacksonParser {
  private ObjectMapper objectMapper;
  public JSONJacksonParser() {
    this.objectMapper = new ObjectMapper();
  }
  public List<Candie> getCandieList(File jsonFile) {
    Candie [] candies = new Candie[0];
    try{
      candies = objectMapper.readValue(jsonFile, Candie[].class);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return Arrays.asList(candies);
  }
}
