package com.kozlov.model.json.gson;

import com.google.gson.stream.JsonReader;
import com.kozlov.model.Candie;
import com.kozlov.model.Value;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
public class JSONGsonReader {
  private JsonReader jsonReader;
  public JSONGsonReader(File json) throws FileNotFoundException {
    jsonReader = new JsonReader(new FileReader(json));
  }
  public List<Candie> readCandie() {
    List<Candie> deviceList = new ArrayList<>();
    Candie candie ;
    try {
      jsonReader.beginArray();
      while (jsonReader.hasNext()) {
        candie = new Candie();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
          String name = jsonReader.nextName();
          if (name.equals("candieNo")) {
            candie.setCandieNo(jsonReader.nextInt());
          }  else if (name.equals("name")) {
            candie.setName(jsonReader.nextString());
          } else if (name.equals("type")) {
            candie.setType(jsonReader.nextString());
          } else if (name.equals("production")) {
            candie.setProduction(jsonReader.nextString());
          } else if (name.equals("vegan")) {
            candie.setVegan(jsonReader.nextBoolean());
          } else if (name.equals("value")) {
            candie.setValue(getValue(jsonReader));
          }//else {
            //jsonReader.skipValue();
          //}
          //deviceList.add(candie);
        }
        deviceList.add(candie);
        jsonReader.endObject();
      }
      jsonReader.endArray();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return deviceList;
  }
  private Value getValue(JsonReader reader) throws IOException {
    Value value = new Value();
    reader.beginObject();
    while (reader.hasNext()) {
      String name = reader.nextName();
      if (name.equals("weight")) {
        value.setWeight(reader.nextDouble());
      } else if (name.equals("carbohydrates")) {
        value.setCarbohydrates(reader.nextDouble());
      } else if (name.equals("protein")) {
        value.setProtein(reader.nextDouble());
      } else if (name.equals("fat")) {
        value.setFat(reader.nextDouble());
      } else if (name.equals("transFat")) {
        value.setTransFat(reader.nextDouble());
      } else if (name.equals("energy")) {
        value.setEnergy(reader.nextInt());
      } else {
        jsonReader.skipValue();
      }
    }
        reader.endObject();
      return value;
    }
}
