package com.kozlov.model.json.gson;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kozlov.model.Candie;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;
public class JSONGsonParser {
  private Gson gson;
  public JSONGsonParser() {
    GsonBuilder gsonBuilder = new GsonBuilder();
    gson = gsonBuilder.create();
  }
  public List<Candie> getCandieList(File json) {
    Candie [] candies = new Candie[0];
    try{
      candies = gson.fromJson(new FileReader(json), Candie[].class);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return Arrays.asList(candies);
  }
  public void writeToFile(List<Candie> devicesList, File json) {
    try (Writer writer = new FileWriter(json)){
      gson.toJson(devicesList, writer);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
