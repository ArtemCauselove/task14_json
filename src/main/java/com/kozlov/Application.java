package com.kozlov;

import com.kozlov.View.View;

public class Application {
    public static void main(String[] args) {
        View view = new View();
        view.start();
    }
}
