package com.kozlov.View;

import com.kozlov.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {
    Controller controller;
    private static Logger logger = LogManager.getLogger(View.class);
     public View(){
        controller = new Controller();
    }
    public void start(){
        logger.info("This is how all parser work\n");
        startJsonHomework();
    }
    public void startJsonHomework(){
        controller.launch();
    }
}
